{
    "name":"group",
    "module":    {
        "name": {
            "type": "string"
        },
        "type": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "status": {
            "type": "string"
        },
        "create_date": {
            "type": "date"
        },
        "create_by": {
            "type": "string"
        },
        "edit_date": {
            "type": "date"
        },
        "edit_by": {
            "type": "string"
        }
    },
    "count":0,
    "items": [
    ]
}
