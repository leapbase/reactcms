{
    "name":"file",
    "module":    {
        "filename": {
            "type": "string"
        },
        "description": {
            "type": "string"
        },
        "path": {
            "type": "string"
        },
        "create_date": {
            "type": "date"
        }
    },
    "count":0,
    "items": [
    ]
}
