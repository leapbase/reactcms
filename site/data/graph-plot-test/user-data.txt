{
    "name":"user",
    "module":    {
        "username": {
            "type": "string",
            "optional": true
        },
        "firstname": {
            "type": "string",
            "optional": true
        },
        "lastname": {
            "type": "string",
            "optional": true
        },
        "email": {
            "type": "string",
            "subtype": {
                "type": "email"
            },
            "optional": false,
            "option": {
                "unique": true
            }
        },
        "salt": {
            "type": "string",
            "subtype": {
                "type": "string"
            }
        },
        "password": {
            "type": "string",
            "subtype": {
                "type": "password"
            }
        },
        "group": {
            "type": "string"
        },
        "status": {
            "type": "string",
            "values": [
                "active",
                "inactive"
            ]
        },
        "create_date": {
            "type": "date"
        },
        "create_by": {
            "type": "string"
        },
        "edit_date": {
            "type": "date"
        },
        "edit_by": {
            "type": "string"
        }
    },
    "count":1,
    "items": [
        {
            "_id": "552b1060dcec2fe009d51f83",
            "username": "admin",
            "email": "admin@leapon.com",
            "password": "d003d6f21b822f38cd5d2ece439a92e95aebea57b872a32d87a6c826534aa1b7081785db4b398c6dd2ffb029c67c4f0f72b8783a7d630b1eda7b5e0c72c44888",
            "salt": 9513022,
            "_class": "user",
            "firstname": "",
            "lastname": "",
            "status": "",
            "group": "",
            "create_date": null,
            "create_by": "",
            "edit_date": null,
            "edit_by": ""
        }
    ]
}
